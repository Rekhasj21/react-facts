
<h1 align="center">React Facts</h1>

## Table of Contents

- [Overview](#overview)
- [Project Link](#project-link)
- [Built With](#built-with)
- [How to use](#how-to-use)
- [Contact](#contact)

## Overview

<a href="" rel="image text"><img src="/react-facts-image.png" alt="" /></a>

- React Facts

## Project Link

<a href="https://rekha-sj-react-facts.netlify.app/" >Click To Check Project Link</a>

### Built With

- HTML
- CSS
- React

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone git@gitlab.com:Rekhasj21/react-facts.git
```

## Contact

- Website (https://rekha-sj-react-facts.netlify.app/)
- GitHub (https://gitlab.com/Rekhasj21/react-facts)