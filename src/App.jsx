import React from 'react'
import Main from './assets/Main'
import Navbar from './assets/Navbar'
import './App.css'

function App() {

  return (
    <div className='app-container'>
    <Navbar />
    <Main />
    </div>
  )

}

export default App
