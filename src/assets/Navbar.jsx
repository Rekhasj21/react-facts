import React from 'react'

export default function Navbar() {
    return (
        <nav className='navbar'>
            <img src='images/react-icon-small.png' className='react-icon' />
            <h3 className='navbar-heading'>ReactFacts</h3>
            <h4 className='navbar-project-heading'>Reat Course- Project 1</h4>
        </nav>
    )
}